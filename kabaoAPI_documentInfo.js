const express = require("express");
const router = express.Router();
module.exports = router;
const mysql = require("mysql");

const db_mysql = mysql.createConnection({
  user: "admin",
  host: "localhost",
  password: "0896390897",
  database: "kabao101",
});

//_________________________________________________________________________________________________________________________ Documents

router.get("/documents", (req, res) => {
  db_mysql.query(
    "SELECT dcm.dcm_id , dcm.dcm_datetime , dcm.dcm_name , dcm.dcm_description , dcm.dcm_content , dcm.sk_id , dcm.t_id , " +
      "sk.sk_name  , CONVERT(sk.sk_cover USING utf8) as sk_cover , sk.sk_cover_type , " +
      "t.t_name , CONVERT(t.t_cover USING utf8) as t_cover , t.t_cover_type , " +
      "skt.skt_name , CONVERT(skt.skt_cover USING utf8) as skt_cover , skt.skt_cover_type , " +
      "lg.lg_name , CONVERT(lg.lg_cover USING utf8) as lg_cover , lg.lg_cover_type " +
      "FROM kb01_document dcm " +
      "LEFT JOIN kb02_skills sk on dcm.sk_id = sk.sk_id " +
      // "LEFT JOIN kb05_skill_tool sk_t on sk.sk_id = sk_t.sk_id " +
      "LEFT JOIN kb03_tools t on dcm.t_id = t.t_id " +
      "LEFT JOIN kb04_languages lg on dcm.lg_id = lg.lg_id " +
      "LEFT JOIN kb02_skills_type skt on sk.skt_id = skt.skt_id order by dcm.dcm_datetime desc",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/documentSearch", (req, res) => {
  let name = req.body.name;
  db_mysql.query(
    "SELECT dcm.dcm_id , dcm.dcm_datetime , dcm.dcm_name , dcm.dcm_description , dcm.dcm_content , dcm.sk_id , dcm.t_id , " +
      "sk.sk_name  , CONVERT(sk.sk_cover USING utf8) as sk_cover , sk.sk_cover_type , " +
      "t.t_name , CONVERT(t.t_cover USING utf8) as t_cover , t.t_cover_type , " +
      "skt.skt_name , CONVERT(skt.skt_cover USING utf8) as skt_cover , skt.skt_cover_type , " +
      "lg.lg_name , CONVERT(lg.lg_cover USING utf8) as lg_cover , lg.lg_cover_type " +
      "FROM kb01_document dcm " +
      "LEFT JOIN kb02_skills sk on dcm.sk_id = sk.sk_id " +
      // "LEFT JOIN kb05_skill_tool sk_t on sk.sk_id = sk_t.sk_id " +
      "LEFT JOIN kb03_tools t on dcm.t_id = t.t_id " +
      "LEFT JOIN kb04_languages lg on dcm.lg_id = lg.lg_id " +
      "LEFT JOIN kb02_skills_type skt on sk.skt_id = skt.skt_id " +
      "WHERE dcm.dcm_name like '%" +
      name +
      "%' order by dcm.dcm_datetime desc",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/documentContent", (req, res) => {
  let id = req.body.id;
  db_mysql.query(
    "SELECT dcm.dcm_id , dcm.dcm_datetime , dcm.dcm_name , dcm.dcm_description , dcm.dcm_content , dcm.dcm_link , dcm.sk_id , dcm.t_id , " +
      "sk.sk_name  , CONVERT(sk.sk_cover USING utf8) as sk_cover , sk.sk_cover_type , " +
      "t.t_name , CONVERT(t.t_cover USING utf8) as t_cover , t.t_cover_type , " +
      "skt.skt_name , CONVERT(skt.skt_cover USING utf8) as skt_cover , skt.skt_cover_type , " +
      "lg.lg_name , CONVERT(lg.lg_cover USING utf8) as lg_cover , lg.lg_cover_type " +
      "FROM kb01_document dcm " +
      "LEFT JOIN kb02_skills sk on dcm.sk_id = sk.sk_id " +
      // "LEFT JOIN kb05_skill_tool sk_t on sk.sk_id = sk_t.sk_id " +
      "LEFT JOIN kb03_tools t on dcm.t_id = t.t_id " +
      "LEFT JOIN kb04_languages lg on dcm.lg_id = lg.lg_id " +
      "LEFT JOIN kb02_skills_type skt on sk.skt_id = skt.skt_id " +
      "WHERE dcm.dcm_id = '" +
      id +
      "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//_________________________________________________________________________________________________________________________ Add document

router.get("/skillType", (req, res) => {
  db_mysql.query(
    "SELECT * , CONVERT(skt_cover USING utf8) as COVER FROM kb02_skills_type",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/skill", (req, res) => {
  var skt = req.body.skt;
  db_mysql.query(
    "SELECT * , CONVERT(sk_cover USING utf8) as COVER FROM kb02_skills  WHERE skt_id = '" +
      skt +
      "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/tool", (req, res) => {
  var sk = req.body.sk;
  db_mysql.query(
    "SELECT * , CONVERT(t_cover USING utf8) as COVER " +
      "FROM kb05_skill_tool sk_t " +
      "LEFT JOIN kb03_tools t ON sk_t.t_id = t.t_id " +
      "WHERE sk_t.sk_id = '" +
      sk +
      "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.get("/allTool", (req, res) => {
  db_mysql.query(
    "SELECT t_id , t_name , t_cover_type , CONVERT(t_cover USING utf8) as t_cover FROM kb03_tools ",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.get("/language", (req, res) => {
  db_mysql.query(
    "SELECT * , CONVERT(lg_cover USING utf8) as COVER FROM kb04_languages",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/sendDocument", (req, res) => {
  var topic = req.body.topic;
  var type = req.body.type;
  var skill = req.body.skill;
  var tool = req.body.tool;
  var lg = req.body.lg;
  var des = req.body.des;
  var link = req.body.link;
  var code = req.body.code;
  const ID = "DCM" + String(Date.now());
  var DateTime = req.body.DateTime;
  // var DateTime = moment().tz("Asia/Bangkok").format("YYYY-MM-DD HH:mm:ss");
  // var date = moment().tz("Asia/Bangkok").format("YYYY-MM-DD");

  db_mysql.query(
    "INSERT INTO kb01_document (dcm_id,dcm_datetime,dcm_name,dcm_description,dcm_content,dcm_link,lg_id,sk_id,t_id) VALUES(?,?,?,?,?,?,?,?,?)",
    [ID, DateTime, topic, des, code, link, lg, skill, tool],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/addType", (req, res) => {
  var type = req.body.type;
  var cover = req.body.cover;
  var img_type = req.body.img_type;
  const ID = "SKT" + String(Date.now());

  db_mysql.query(
    "INSERT INTO kb02_skills_type (skt_id,skt_name,skt_cover,skt_cover_type) VALUES(?,?,?,?)",
    [ID, type, cover, img_type],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/addSkill", (req, res) => {
  var type = req.body.type;
  var skill = req.body.skill;
  var cover = req.body.cover;
  var img_type = req.body.img_type;
  const ID = "SK" + String(Date.now());
  // var DateTime = moment().tz("Asia/Bangkok").format("YYYY-MM-DD HH:mm:ss");
  var DateTime = req.body.DateTime;

  db_mysql.query(
    "INSERT INTO kb02_skills (sk_id,sk_datetime,sk_name,skt_id,sk_cover,sk_cover_type) VALUES(?,?,?,?,?,?)",
    [ID, DateTime, skill, type, cover, img_type],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/addTool", (req, res) => {
  var tool = req.body.tool;
  var cover = req.body.cover;
  var img_type = req.body.img_type;
  const ID = "T" + String(Date.now());

  db_mysql.query(
    "INSERT INTO kb03_tools (t_id,t_name,t_cover,t_cover_type) VALUES(?,?,?,?)",
    [ID, tool, cover, img_type],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/addLg", (req, res) => {
  var lg = req.body.lg;
  var cover = req.body.cover;
  var img_type = req.body.img_type;
  const ID = "LG" + String(Date.now());

  db_mysql.query(
    "INSERT INTO kb04_languages (lg_id,lg_name,lg_cover,lg_cover_type) VALUES(?,?,?,?)",
    [ID, lg, cover, img_type],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/matchTool", (req, res) => {
  var tool = req.body.tool;
  var skill = req.body.skill;
  const ID = "SK_T" + String(Date.now());

  db_mysql.query(
    "INSERT INTO kb05_skill_tool (sk_t_id,sk_id,t_id) VALUES(?,?,?)",
    [ID, skill, tool],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//_________________________________________________________________________________________________________________________
// now use `db` to make your queries
