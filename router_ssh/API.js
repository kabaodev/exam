var mysql2 = require("mysql2");
var SSH2Client = require("ssh2").Client;

const express = require("express");
const router = express.Router();
module.exports = router;

var sshConf = {
  host: "45.91.132.72",
  port: 22,
  username: "root",
  password: "@Tom0896390897kabao",
};
var sqlConf = {
  user: "admin",
  password: "0896390897",
  database: "dudee_exam_db",
  timeout: 100000,
};

var ssh = new SSH2Client();
ssh.on("ready", function () {
  ssh.forwardOut("127.0.0.1", 24000, "127.0.0.1", 3306, function (err, stream) {
    if (err) throw err;

    sqlConf.stream = stream;
    var db_mysql = mysql2.createConnection(sqlConf);

    //_________________________________________________________________________________________________________________________

    // const line = require("@line/bot-sdk");

    require("dotenv").config();
    const momemt = require("moment-timezone");
    var cron = require("node-cron");

    const middleware = (req, res, next) => {
      if (req.headers.authorization === process.env.TOKEN) next();
      else res.send("not allowed");
    };

    var notify_list = [];

    // Manage washing machine _____________________________________________________________
    router.post("/add_machine", middleware, (req, res) => {
      var machine_id = "WSMC" + String(Date.now());
      let machine_name = req.body.machine_name;
      let machine_status = "empty";
      db_mysql.query(
        "INSERT INTO washing_mc_info (machine_id,machine_name,machine_status) VALUES(?,?,?)",
        [machine_id, machine_name, machine_status],
        (err, result) => {
          if (err) {
            console.log(err);
          } else {
            res.send(result);
          }
        }
      );
    });

    router.put("/edit_machine", middleware, (req, res) => {
      var machine_id = req.body.machine_id;
      let machine_name = req.body.machine_name;
      let machine_status = req.body.machine_status;
      let pay_method = req.body.pay_method;
      let start_time = req.body.start_time;
      let end_time = req.body.end_time;
      let wash_time = req.body.wash_time;

      db_mysql.query(
        "UPDATE washing_mc_info " +
          "SET machine_name = '" +
          machine_name +
          "' , machine_status = '" +
          machine_status +
          "' , pay_method = '" +
          pay_method +
          "' , start_time = '" +
          start_time +
          "' , end_time = '" +
          end_time +
          "' , wash_time = " +
          wash_time +
          " WHERE machine_id = '" +
          machine_id +
          "'",
        (err, result) => {
          if (err) {
            console.log(err);
          } else {
            res.send(result);
          }
        }
      );
    });

    router.delete("/delete_machine/:id", middleware, (req, res) => {
      let machine_id = req.params.id;
      db_mysql.query(
        "DELETE FROM washing_mc_info " +
          "WHERE machine_id = '" +
          machine_id +
          "'",
        (err, result) => {
          if (err) {
            console.log(err);
          } else {
            res.send(result);
          }
        }
      );
    });

    router.get("/machine_info", middleware, (req, res) => {
      db_mysql.query("SELECT * FROM washing_mc_info", (err, result) => {
        if (err) {
          console.log(err);
        } else {
          res.send(result);
        }
      });
    });

    // Start machine _____________________________________________________________
    router.get(
      "/get_start/:machine_id/:method/:wash_time",
      middleware,
      (req, res) => {
        var machine_id = req.params.machine_id;
        let method = req.params.method;
        let start_time = momemt().format("YYYY-MM-DD HH:mm:ss");
        let wash_time = Number(req.params.wash_time);
        let end_time = momemt()
          .add(wash_time, "minutes")
          .format("YYYY-MM-DD HH:mm:ss");

        // // Update to database
        if (method == "coin") {
          var send_status = {
            machine_id: machine_id,
            status: "OK Start",
            start_time: start_time,
            end_time: end_time,
            wash_time: wash_time,
          };
          db_mysql.query(
            "SELECT * FROM washing_mc_info WHERE machine_id ='" +
              machine_id +
              "'",
            (err, result) => {
              if (err) {
                console.log(err);
              } else {
                let status = result[0].machine_status;
                if (status == "working") {
                  res.send("Machine " + status);
                } else {
                  // res.send("Machine " + status);
                  updateMachine_Status(
                    "working",
                    method,
                    start_time,
                    end_time,
                    wash_time,
                    machine_id
                  );
                  res.send(send_status);
                  notify(machine_id, result[0].machine_name, end_time);
                }
              }
            }
          );
        } else if (method == "pqyment-gateway") {
          res.send("Generate QR Code");
        }
      }
    );

    // Update washing machine status
    function updateMachine_Status(
      machine_status,
      pay_method,
      start_time,
      end_time,
      wash_time,
      machine_id
    ) {
      db_mysql.query(
        "UPDATE washing_mc_info " +
          "SET machine_status = '" +
          machine_status +
          "' , pay_method = '" +
          pay_method +
          "' , start_time = '" +
          start_time +
          "' , end_time = '" +
          end_time +
          "' , wash_time = " +
          wash_time +
          " WHERE machine_id = '" +
          machine_id +
          "'",
        (err, result) => {
          if (err) {
            console.log(err);
          } else {
            console.log("Update success");
          }
        }
      );
    }

    // Set notification ( under 1 minute )
    function notify(machine_id, machine_name, end_time) {
      let noti_time = momemt(end_time)
        .subtract(1, "minutes")
        .format("ss mm HH");
      let today = momemt().format("YYYY-MM-DD HH:mm:ss");
      cron.schedule(noti_time + " * * *", () => {
        console.log(
          "RUN ->> ",
          machine_id + " - " + machine_name + " - " + noti_time
        );
        LineNotify(
          machine_name + " : อีก 1 นาทีเครื่องซักผ้าจะสิ้นสุดการทำงาน"
        );
        updateMachine_Status("empty", "", today, today, 0, machine_id);
      });
      console.log(
        "SET ->> ",
        machine_id + " - " + machine_name + " - " + noti_time
      );
    }

    // Get machine status

    // Under 1 min -> Noti to LINE Group
    //----------- LINE NOTIFY --------------------
    const lineNotify = require("line-notify-nodejs")(
      "SeUrCnf3J9dsa0z04flDxbNoHoraV6p4K6Whgd2OiHJ"
    );
    function LineNotify(Message) {
      lineNotify
        .notify({
          message: Message,
        })
        .then(() => {
          console.log("send completed!");
        });
    }

    //_________________________________________________________________________________________________________________________
  });
});
ssh.connect(sshConf);
